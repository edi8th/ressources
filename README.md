|Ressources Podcast / Articles / Livres / etc...|
|---|
|**Podcasts**|
|[Les couilles sur la table](https://soundcloud.com/lescouilles-podcast)|
|[ArteRadio](https://www.arteradio.com)|
|**Emissions**|
|[P.Picasso : «Quand je n’ai pas de bleu, je mets du rouge»](https://www.franceculture.fr/emissions/3-minutes-de-philosophie-pour-redevenir-humain/picasso)|
|[Narcisse, accusé non coupable](https://www.franceculture.fr/emissions/narcisse-accuse-juge-non-coupable/saison-02-07-2018-26-08-2018)|
|**Chaine Youtube**|
|[Alt236](https://www.youtube.com/channel/UC1KxoDAzbWOWOhw5GbsE-Bw)|
|[Bill Wurtz](https://www.youtube.com/channel/UCq6aw03lNILzV96UvEAASfQ)|
|[DataGueule](https://www.youtube.com/channel/UCm5wThREh298TkK8hCT9HuA)|
|[Dirty Biology](https://www.youtube.com/channel/UCtqICqGbPSbTN09K1_7VZ3Q)|
|[Hacking Social](https://www.youtube.com/channel/UCGeFgMJfWclTWuPw8Ok5FUQ)|
|[L'école du chat noir](https://www.youtube.com/channel/UCqJ4PgXsd2KFY2K2Vldc-MQ)|
|[Le Stagirite](https://www.youtube.com/channel/UCtyONQLs6htK-wWiCAk4wVw)|
|[Professeur Feuillage](https://www.youtube.com/channel/UCGl2QLR344ry4Y20RV9dM3g)|
|[StripTease](https://www.youtube.com/channel/UCTljLU4Tb2jcW9n23APDwZg)|
|[Thinkerview](https://www.youtube.com/channel/UCQgWpmt02UtJkyO32HGUASQ)|
|**Videos**|
|[G.Deleuze: «Qu'est ce que l'acte de création?»](https://www.youtube.com/watch?v=2OyuMJMrCRw&t=881s)|
|[P.Thiellement : Conférence sur le Grand Jeu](https://www.youtube.com/watch?v=eetwb8dzLVk&list=PL0xGcnuTPRk401AD9MIVBzSfyMDk6bljs)|
|[B.Wurtz: History of the entire world, i guess](https://www.youtube.com/watch?v=xuCn8ux2gbs)
|**High Level**|
|[La célébration des signes : pratiques de l'écriture énigmatique en Égypte ancienne](https://www.college-de-france.fr/site/jean-luc-fournet/symposium-2018-12-07-10h05.htm)|
|[Language evolution](https://blogs.ntu.edu.sg/hss-language-evolution/)|
|**Livres (par ordre de difficulté)**|
|[E.Lupton : Comprendre la typographie](https://catalogue.bnf.fr/ark:/12148/cb44371618z)|
|[G.Unger : Pendant la lecture](https://catalogue.bnf.fr/ark:/12148/cb44264907g)|
|[J.Hochuli : L'abécédaire d'un typographe](https://catalogue.bnf.fr/ark:/12148/cb44312933p)|
|[Roger Excoffon et la Fonderie Olive](https://catalogue.bnf.fr/ark:/12148/cb42316302f)|
|[A.Frutiger : L'homme et ses signes](https://catalogue.bnf.fr/ark:/12148/cb37113346k)|
|[J.Tschichold : Livre et typographie](https://catalogue.bnf.fr/ark:/12148/cb45449655s)|
|[J.Hochuli : Le détail en typographie](https://catalogue.bnf.fr/ark:/12148/cb44458099k)|
|[A.Frutiger : Une vie consacrée à l'écriture typographique](https://catalogue.bnf.fr/ark:/12148/cb391651386)|
|[E.Gill : Un essai sur la typographie](https://catalogue.bnf.fr/ark:/12148/cb454917435)|
|[F.Smeijer : Les contrepoinçons](https://editions-b42.com/produit/les-contrepoincons/)|
|[R.Kinross : La typographie moderne](https://catalogue.bnf.fr/ark:/12148/cb45706988j)|